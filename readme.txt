-------------------------------------------------------------------
-- ALUNOS                                                        --
-------------------------------------------------------------------
 - João Gustavo Pauluk  : GRR20120704
 - Michele Macedo Weber : GRR20123974

-------------------------------------------------------------------
-- ESTRUTURAS DE DADOS                                           --
-------------------------------------------------------------------
Estruturas de dados utilizadas:
1. struct grafo
2. struct vertice
3. struct aresta
4. struct lista

Abaixo explicaremos somente os pontos sobre as estruturas de dados mais relevantes:
2. vertice
  visitado : Além de sua funcao comentada no trabalho 2, no trabalho 3 é utilizado para marcar quais vertices já foram visitados durante a busca de uma caminho aumentante
  coberto: Utilizado para indicar se um vértice está ou nao coberto

3. aresta
Esta estrutura de dado não estava prevista no grafo.h e foi adicionada.
  visitado : Idem ao da estrutura vertice
  coberto : Idem ao da estrutura vertice

4. lista
Não foi realizada mudanças em relação ao produzido no trabalho 2


-------------------------------------------------------------------
-- EMPARELHAMENTO MÁXIMO                                         --
-------------------------------------------------------------------
  - A função de emparelhamento máximo foi implementada baseada na solução providenciado pelo monitor da disciplina.

-------------------------------------------------------------------
-- DEMAIS COMENTÁRIOS                                            --
-------------------------------------------------------------------
  - Os warnings de compilação relacionados a falta de um protótipo foram resolvidos por inserir os prótipos no grafo.h, uma vez que este já está sendo enviado para que a estrutura aresta funcione.
  - A função retira_no foi implementado ao invés de remove_no, pois já haviamos implementado esta função antes da especificação do trabalho 3, além de acharmos mais conveniente procurar um nó pelo seu conteúdo, ao invés do endereco de um nó da lista.
  - Durante os testes do programa, foi verificado se houve perdas de memórias através da ferramente Valgrind.

