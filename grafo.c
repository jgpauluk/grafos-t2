#include <stdio.h>
#include "grafo.h"
#include <malloc.h>
#include <graphviz/cgraph.h>
#include <string.h>
#include <stdlib.h>

#define DEF_WEIGHT 0

struct grafo {
    char *nome;
    int ponderado; //1 se ponderado, 0 caso contrário
    int direcionado; //1 se direcionado, 0 caso contrário
    lista vertices;
};

struct vertice {
    char *nome;
    lista arestas_saida;
    lista arestas_entr;
    int num_aresta_saida;
    int num_aresta_entr;
    unsigned int *rotulo;
    int numRotulo;
    unsigned int ordem;
    lista colVert; /* Colecao de vertices: utilizado na função de ordem perfeita */
    int visitado;
    int coberto;
};

struct aresta {
    char *nome;
    vertice vert_inicio;
    vertice vert_fim;
    int visitado;
    int peso;
    int coberto;
};

struct no {

  void *conteudo;
  no proximo;
};

struct lista {
  unsigned int tamanho;
  no primeiro;
};
//---------------------------------------------------------------------------
// devolve o número de nós da lista l

unsigned int tamanho_lista(lista l) { return l->tamanho; }

//---------------------------------------------------------------------------
// devolve o primeiro nó da lista l,
//      ou NULL, se l é vazia

no primeiro_no(lista l) { return l->primeiro; }

void set_primeiro_no(lista l, no n) { l->primeiro = n; }

//---------------------------------------------------------------------------
// devolve o conteúdo do nó n
//      ou NULL se n = NULL 

void *conteudo(no n) { return n->conteudo; }

//---------------------------------------------------------------------------
// devolve o sucessor do nó n,
//      ou NULL, se n for o último nó da lista

no proximo_no(no n) { return n->proximo; }

void set_proximo_no(no n, no prox) { n->proximo = prox; }

lista constroi_lista(void) {

  lista l = malloc(sizeof(struct lista));

  if ( ! l )
    return NULL;

  l->primeiro = NULL;
  l->tamanho = 0;

  return l;
}

int destroi_lista(lista l, int destroi(void *)) {

  no p;
  int ok=1;

  while ( (p = primeiro_no(l)) ) {

    l->primeiro = proximo_no(p);

    if ( destroi )
      ok &= destroi(conteudo(p));

    free(p);
  }

  free(l);

  return ok;
}

no insere_lista(void *conteudo, lista l) {

  no novo = malloc(sizeof(struct no));

  if ( ! novo )
    return NULL;

  novo->conteudo = conteudo;
  novo->proximo = primeiro_no(l);
  ++l->tamanho;

  return l->primeiro = novo;
}

int retira_no(void *cont, lista l) {
    no n, nAnt;

    if(!l || !conteudo(primeiro_no(l)))
        return 0;

    if(cont == conteudo(primeiro_no(l))) {
        n = proximo_no(primeiro_no(l));
        free(primeiro_no(l));
        l->primeiro = n;
    }

    nAnt = primeiro_no(l);
    for(n = primeiro_no(l); n; n = proximo_no(n)) {
        if(conteudo(n) == cont) {
            nAnt->proximo = proximo_no(n);
            free(n);
            break;
        }
        nAnt = n;

    }

    l->tamanho--;
}

char *nome_grafo(grafo g) {
    return g->nome;
}

int direcionado(grafo g) {
    return g->direcionado;
}

int ponderado(grafo g) {
    return g->ponderado;
}

unsigned int n_vertices(grafo g) {
    if(g->vertices == NULL)
        return 0;
    return tamanho_lista(g->vertices);
}

lista vertices(grafo g) {
    return g->vertices;
}

/* Percorre os vértices e verifica quantas arestas possuem
     O algoritmo percorre todos as arestas de saída de um vértice e soma um ao
   totalizador.
     Se for um grafo direcionado, a somatório dos graus de um vértice é o nú-
   mero de suas arestas.
     Senão, o somatório dos graus de um vértices é o dobro do número de suas 
   arestas.
*/
unsigned int n_arestas(grafo g) {
    unsigned int tam = 0;
    vertice v;
    no no_v, no_a;

    //percorre lista de vertices de um grafo
    for(no_v = primeiro_no(g->vertices); no_v; no_v = proximo_no(no_v)) {
        v = conteudo(no_v);
        
        //percorre lista de arestas de um vértice
        for(no_a = primeiro_no(v->arestas_saida); no_a; no_a = proximo_no(no_a)) {
            tam++;
        }
    }

    if(!direcionado(g))
        return tam/2;

    return tam;
}

char *nome_vertice(vertice v) {
    return v->nome;
}

char *nome_aresta(aresta a) {
    return a->nome;
}

grafo aloca_grafo(char *nome, int ponderado, int direcionado) {
    grafo g;

    g = malloc(sizeof(struct grafo));

    if(!g)
        return NULL;

    g->nome = malloc(strlen(nome) + 1);
    strcpy(g->nome, nome);

    g->ponderado = ponderado;
    g->direcionado = direcionado;
    g->vertices = constroi_lista();

    return g;
}

vertice aloca_vertice(char *nome, int direcionado) {
    vertice v;
    
    v = malloc(sizeof(struct vertice));
    if(!v)
        return NULL;

    v->nome = malloc(strlen(nome) + 1);
    strcpy(v->nome, nome);

    v->visitado = 0;
    v->coberto = 0;
    v->rotulo = NULL;
    v->numRotulo = 0;
    v->colVert = NULL;
    v->arestas_saida = constroi_lista();
    if(direcionado) 
        v->arestas_entr = constroi_lista();
    else
        v->arestas_entr = v->arestas_saida;

    return v;
}

aresta aloca_aresta(vertice inicio, vertice fim, int peso) {
    aresta a;

    a = malloc(sizeof(struct aresta));
    if(!a)
        return NULL;

    /*a->nome = malloc(strlen(nome) + 1);
    strcpy(a->nome, nome);*/
    a->peso = peso;    
    a->vert_inicio = inicio;
    a->vert_fim = fim;
    a->visitado = 0;
    a->coberto = 0;

    return a;    
}

vertice encontra_vertice_nome(grafo g, char *nome) {
    no n;

    for(n = primeiro_no(g->vertices); n; n = proximo_no(n)) {
        if(!strcmp(nome_vertice(conteudo(n)), nome))
            return conteudo(n);
    }
    printf("Erro ao encontrar o vértice\n");
    return NULL;
}

int existe_aresta(vertice v1, vertice v2) {
    no n;
    aresta a;

    for(n = primeiro_no(v1->arestas_saida); n; n = proximo_no(n)) {
        a = conteudo(n);
        if(a->vert_inicio == v1 && a->vert_fim == v2)
            return 1;
    }
    return 0;
    
}

int existe_aresta_nao_direcionado(vertice v1, vertice v2) {
    no n;
    aresta a;

    for(n = primeiro_no(v1->arestas_saida); n; n = proximo_no(n)) {
        a = conteudo(n);
        if((a->vert_inicio == v1 && a->vert_fim == v2) || (a->vert_inicio == v2 && a->vert_fim == v1))
            return 1;
    }
    return 0;
}

grafo le_grafo(FILE *input) {
    grafo p_grafo;
    Agraph_t *cg_grafo;

    vertice p_vertice, p_vertice2;
    Agnode_t *cg_vertice;

    aresta p_aresta;
    Agedge_t *cg_aresta;

    char * peso;

    //Inicializa grafos
    cg_grafo = agread(input, NULL);
    //agwrite(cg_grafo, stdout);
    p_grafo = aloca_grafo(agnameof(cg_grafo), 0, agisdirected(cg_grafo));
    
    //Inicializa vértices do grafo p_grafo
    for(cg_vertice = agfstnode(cg_grafo); cg_vertice; cg_vertice = agnxtnode(cg_grafo, cg_vertice)) {
        p_vertice = aloca_vertice(agnameof(cg_vertice), agisdirected(cg_grafo));
        if(!p_vertice) {
            printf("Houve um erro ao alocar o vértice %s", agnameof(cg_grafo));
            return NULL;
        }

        insere_lista(p_vertice, p_grafo->vertices);
    }

    //Se for direcionado, percorre a lista de vértices através das funções
    //agfstin, agnxtin, agfstout, agnxtout
    if(agisdirected(cg_grafo)) {
        for(cg_vertice = agfstnode(cg_grafo); cg_vertice; cg_vertice = agnxtnode(cg_grafo, cg_vertice)) {
            p_vertice = encontra_vertice_nome(p_grafo, agnameof(cg_vertice));
            for(cg_aresta = agfstin(cg_grafo, cg_vertice); cg_aresta; cg_aresta = agnxtin(cg_grafo, cg_aresta)) {
                p_vertice2 = encontra_vertice_nome(p_grafo, agnameof(agtail(cg_aresta)));

                peso = agget(cg_aresta, (char *)"peso");
                p_aresta = aloca_aresta(p_vertice2, p_vertice, peso ? atoi(peso) : 0); 
                p_grafo->ponderado = peso ? 1 : 0;

                //p_vertice : vertice em que a aresta chega
                //p_vertice2: vertice em que a aresta sai
                insere_lista(p_aresta, p_vertice->arestas_entr);
                insere_lista(p_aresta, p_vertice2->arestas_saida);
            }
        }
    }
    else {
        for(cg_vertice = agfstnode(cg_grafo); cg_vertice; cg_vertice = agnxtnode(cg_grafo, cg_vertice)) {
            for(cg_aresta = agfstedge(cg_grafo, cg_vertice); cg_aresta; cg_aresta = agnxtedge(cg_grafo, cg_aresta, cg_vertice)) {
                p_vertice = encontra_vertice_nome(p_grafo, agnameof(agtail(cg_aresta)));
                p_vertice2 = encontra_vertice_nome(p_grafo, agnameof(aghead(cg_aresta)));
                
                if(!existe_aresta_nao_direcionado(p_vertice, p_vertice2)) {
                    peso = agget(cg_aresta, (char *)"peso");
                    p_aresta = aloca_aresta(p_vertice, p_vertice2, peso ? atoi(peso) : 0); 
                    p_grafo->ponderado = peso ? 1 : 0;

                    insere_lista(p_aresta, p_vertice->arestas_entr);
                    insere_lista(p_aresta, p_vertice2->arestas_entr);
                }
            }
        }
    }

    //Desaloca grafo
    agclose(cg_grafo);

    return p_grafo;
}

grafo escreve_grafo(FILE *output, grafo g) {
    char ponta;
    no n, m;
    aresta a;
    vertice v;

    if(g->direcionado)
        ponta = '>';
    else
        ponta = '-';

    fprintf(output, direcionado(g) ? "digraph %s {\n" : "graph %s {\n", nome_grafo(g));

    //Imprime vertices
    for(n = primeiro_no(g->vertices); n; n = proximo_no(n)) {
        v = conteudo(n);
        printf("  %s;\n", nome_vertice(v));
    }

    for(n = primeiro_no(g->vertices); n; n=proximo_no(n)) {
        v = conteudo(n);
        for(m = primeiro_no(v->arestas_saida); m; m = proximo_no(m)) {
            a = conteudo(m);
            if(!a->visitado) {
                if(ponderado(g))
                    fprintf(output, "  \"%s\" -%c \"%s\" [peso=%d];\n", nome_vertice(a->vert_inicio), ponta, nome_vertice(a->vert_fim), a->peso);
                else
                    fprintf(output, "  \"%s\" -%c \"%s\";\n", nome_vertice(a->vert_inicio), ponta, nome_vertice(a->vert_fim));
                a->visitado = direcionado(g) ? 0 : 1;
            }
            else
                a->visitado = 0;
        }

    }

    fprintf(output, "}\n");
    return g;
}

int destroi_aresta(void *a) {
    int ok = 1;
    aresta ar = a;

    if(ar->visitado)
        free(ar);
    else
        ar->visitado = 1;

    return ok;    
}

int destroi_vertice(void *v) {
    int ok = 1;
    vertice vert = v;

    free(vert->nome);
    if(vert) {
        ok &= destroi_lista(vert->arestas_saida, destroi_aresta);
        if(vert->arestas_saida != vert->arestas_entr)
            ok &= destroi_lista(vert->arestas_entr, destroi_aresta);
    }
    free(vert);

    return ok;
}

int destroi_grafo(void *g) {
    int ok = 1;
    grafo gr = g;

    free(gr->nome);
    ok &= destroi_lista(gr->vertices, destroi_vertice);

    free(g);

    return ok;
}

grafo copia_grafo(grafo g) {
    grafo novoG;

    vertice v, vFim, vIni;
    aresta a, novoA;
    no noV, noA;

    novoG = aloca_grafo(g->nome, ponderado(g), direcionado(g));

    //Aloca os vértices
    for(noV = primeiro_no(g->vertices); noV; noV = proximo_no(noV)) {
        v = conteudo(noV);
        insere_lista(aloca_vertice(nome_vertice(v), direcionado(g)), novoG->vertices);
    }
        

    for(noV = primeiro_no(g->vertices); noV; noV = proximo_no(noV)) {
        v = conteudo(noV);
        for(noA = primeiro_no(v->arestas_saida); noA; noA = proximo_no(noA)) {
            a = conteudo(noA);
            if(!a->visitado) {
                vFim = encontra_vertice_nome(novoG, nome_vertice(a->vert_fim));
                vIni = encontra_vertice_nome(novoG, nome_vertice(a->vert_inicio));
                novoA = aloca_aresta(vIni, vFim, a->peso);
                insere_lista(novoA, vFim->arestas_entr);
                insere_lista(novoA, vIni->arestas_saida);

                //Se o grafo for direcionado, não altera o visitado, por ele só passará uma vez por cada arestas.
                a->visitado = direcionado(g) ? 0 : 1;
            }
            else
                a->visitado = 0;
        }
    }
    

    return novoG;
}

lista vizinhanca(vertice v, int direcao, grafo g) {
    no n;
    aresta a;
    lista l = constroi_lista();

    if(direcao == 0)
        for(n = primeiro_no(v->arestas_saida); n; n = proximo_no(n)) {
            a = conteudo(n);
            insere_lista(v == a->vert_fim ? a->vert_inicio : a->vert_fim, l);
        }
    else if(direcao == 1) {
        for(n = primeiro_no(v->arestas_entr); n; n = proximo_no(n)) {
            a = conteudo(n);
            insere_lista(a->vert_inicio, l);

            //Testa para ver se deu certo a vizinhança ou nao.
            if(a->vert_inicio == v)
                printf("Erro encontrar vizinhança grafo direcionado");
        }
    }
    else
        for(n = primeiro_no(v->arestas_saida); n; n = proximo_no(n)) {
            a = conteudo(n);
            insere_lista(a->vert_fim, l);
        }

    return l;
}

unsigned int grau(vertice v, int direcao, grafo g) {
    lista l = vizinhanca(v, direcao, g);
    unsigned int gr = tamanho_lista(l);

    destroi_lista(l, NULL);

    return gr;
}

int sao_vizinhos(vertice v1, vertice v2) {
    no n;
    aresta a;

    for(n = primeiro_no(v1->arestas_saida); n; n = proximo_no(n)) {
        a = conteudo(n);
        if((a->vert_inicio == v1 && a->vert_fim == v2) || (a->vert_inicio == v2 && a->vert_fim == v1))
            return 1;
    }

    return 0;
}



int clique(lista l, grafo g) {
    vertice v1, v2;
    no n1, n2;    

    for(n1 = primeiro_no(l); n1; n1 = proximo_no(n1)) {
        v1 = conteudo(n1);
        for(n2 = primeiro_no(l); n2; n2 = proximo_no(n2)) {
            v2 = conteudo(n2);
            //v1 não precisa estar na sua própria vizinhança
            if(v1 == v2)
                continue;
            if(direcionado(g)) {
                if(!existe_aresta(v1, v2))
                    return 0;
            }
            if(!sao_vizinhos(v1, v2))
                return 0;
            
        }
    }

    return 1;
}

int simplicial(vertice v, grafo g) {
    int simp = 0;
    lista l = vizinhanca(v, 0, g);

    if(clique(l, g))
        simp = 1;
    
    destroi_lista(l, NULL);
    
    return simp;
}

/*Retorna o vértice com o maior rótulo dentro de uma lista de vértices */
vertice retorna_maior_rotulo(lista l) {
    no n;
    vertice v, maiorV;
    int i;

    maiorV = conteudo(primeiro_no(l));
    for(n = primeiro_no(l); n; n = proximo_no(n)) {
        v = conteudo(n);
        for(i = 0; i < maiorV->numRotulo && i < v->numRotulo && maiorV->rotulo[i] == v->rotulo[i]; i++);
        if(maiorV->numRotulo < (i + 1) && v->numRotulo >= (i + 1))
            maiorV = v;
        if(maiorV->numRotulo >= (i + 1) && v->numRotulo >= (i + 1) && v->rotulo[i] > maiorV->rotulo[i])
            maiorV = v;
    }

    return maiorV;

}

/*Verifica se o vértice v existe na lista l*/
int existe_vertice(vertice v, lista l) {
    no n;

    for(n = primeiro_no(l); n; n = proximo_no(n)) {
        if(conteudo(n) == v)
            return 1;
    }
    return 0;
}


/* Função auxiliar para acompanhar a execucao de busca_largura_lexicografica*/
void imprime_vertices(grafo g) {
    no n;
    vertice v;
    int i;

    for(n = primeiro_no(g->vertices); n; n = proximo_no(n)) {
        v = conteudo(n);
        printf("%s", nome_vertice(v));
        if(v->numRotulo)
            for(i = 0; i < v->numRotulo; i++)
                printf("%d ", v->rotulo[i]);
        printf("\n");

    }
}

lista busca_largura_lexicografica(grafo g) {
    no n;
    vertice v, vertViz;
    lista l = constroi_lista(), viz, ordem_lex = constroi_lista();

    //Inicialização da lista de vértices
    for(n = primeiro_no(g->vertices); n; n = proximo_no(n)) {
        v = conteudo(n);
        insere_lista(v, l);

        //Se o rótulo já estiver inicializado, libera
        if(v->rotulo) {
            free(v->rotulo);
            v->rotulo = NULL;
        }

        v->rotulo = malloc(sizeof(int) * tamanho_lista(g->vertices));
    }

    while(tamanho_lista(l)) {
        v = retorna_maior_rotulo(l);
        insere_lista(v, ordem_lex);
        v->ordem = tamanho_lista(l);

        viz = vizinhanca(v, 0, g);
        retira_no(v, l);

        /* Para cada vizinho de v, adiciona o rótulo */
        for(n = primeiro_no(viz); n; n = proximo_no(n)) {
            vertViz = conteudo(n);
            /* Verifica se o vértice existe na lista */
            if(existe_vertice(vertViz, l)) {
                vertViz->rotulo[vertViz->numRotulo] = tamanho_lista(l);
                vertViz->numRotulo++;
            }
        }
        destroi_lista(viz, NULL);
    }

    destroi_lista(l, NULL);
    
    /* Destroi rotulos */    
    for(n = primeiro_no(g->vertices); n; n = proximo_no(n)) {
        v = conteudo(n);
        free(v->rotulo);
    }

    return ordem_lex;

}

/*Remove vértice de um grafo nao direcionado */
void remove_vertice_grafo(grafo g, vertice v) {
    vertice v2;
    aresta a;
    no n;

    for(n = primeiro_no(v->arestas_saida); n; n = proximo_no(n)) {
        a = conteudo(n);
        v2 = a->vert_fim == v ? a->vert_inicio : a->vert_fim;
        retira_no(a, v2->arestas_saida);
    }
    destroi_lista(v->arestas_saida, destroi_aresta);
    retira_no(v, g->vertices);
    free(v);
}

vertice retorna_menor_rotulo(lista l) {
    no n;
    vertice v, menorV;

    menorV = conteudo(primeiro_no(l));
    for(n = primeiro_no(l); n; n = proximo_no(n)) {
        v = conteudo(n);
        if(v->visitado)
            continue;
        
        if(menorV->ordem > v->ordem)
            menorV = v;
    }

    return menorV;
}

/* Funcao auxiliar para ordem_perfeita_eliminação
   Adiciona a vizinhanca não visitada de v em u */
void adiciona_vizinhanca_em_vertice(vertice v, vertice u) {
    lista l;
    no n;
    vertice x;

    l = vizinhanca(v, 0, NULL); /*TODO: VER COMO RESOLVER ESSE NULL*/
    for(n = primeiro_no(l); n; n = proximo_no(n)) {
        x = conteudo(n);

        if(!x->visitado && x != u)  {
            insere_lista(x, u->colVert);
        }
    }

    destroi_lista(l, NULL);
}

int existe_vertice_lista(lista l, vertice v) {
    no n;

    for(n = primeiro_no(l); n; n = proximo_no(n)) {
        if(conteudo(n) == v)
            return 1;
    }
    
    return 0;
}

int verifica_colecao_vertices(grafo g, vertice v) {
    no n;
    lista viz = vizinhanca(v, 0, g);

    for(n = primeiro_no(v->colVert); n; n = proximo_no(n)) {
        if(!existe_vertice_lista(viz, conteudo(n))) {
            destroi_lista(viz, NULL);
            return 0;
        }
    }

    destroi_lista(viz, NULL);    

    return 1;    
}

/* Destroi e reinicializa o grafo para o estado inicial */
void finaliza_ordem_perfeita(grafo g) {
    no n;
    vertice v;

    for(n = primeiro_no(g->vertices); n; n= proximo_no(n)) {
        v = conteudo(n);

        v->visitado = 0;
        destroi_lista(v->colVert, NULL);
    }
}

int ordem_perfeita_eliminacao(lista l, grafo g) {
    no n;
    vertice v, menorRotV;
    lista viz;

    /* Inicializa colVert */
    for(n = primeiro_no(l); n; n = proximo_no(n)) {
        v = conteudo(n);
        v->colVert = constroi_lista();
    }

    for(n = primeiro_no(l); n; n = proximo_no(n)) {
        v = conteudo(n);

        viz = vizinhanca(v, 0, g);        
        menorRotV = retorna_menor_rotulo(viz);
        destroi_lista(viz, NULL);

        adiciona_vizinhanca_em_vertice(v, menorRotV);

        if(!verifica_colecao_vertices(g, v)) {
            finaliza_ordem_perfeita(g);
            return 0;
        }

        v->visitado = 1;
    }

    finaliza_ordem_perfeita(g);    
 
    return 1;
}

int cordal(grafo g) {
    lista ordem_lex;

    ordem_lex = busca_largura_lexicografica(g);
    if(ordem_perfeita_eliminacao(ordem_lex, g)) {
        destroi_lista(ordem_lex, NULL);
        return 1;
    }

    destroi_lista(ordem_lex, NULL);
    return 0;
}

int busca_caminho(vertice v, lista l, int last) {
    no n;
    aresta a;
    vertice vizinho;

    if (!v->coberto && !v->visitado)
        return 1;
    v->visitado = 1;
    for(n = primeiro_no(v->arestas_saida); n; n = proximo_no(n)) {
        a = conteudo(n);
        vizinho = a->vert_inicio != v ? a->vert_inicio : a->vert_fim;

        if(a->coberto != last)
            if (!vizinho->visitado && busca_caminho(vizinho, l, !last)) {
                insere_lista(a, l);
                return 1;
            }
    }
    return 0;
}

void desvisita_vertices(grafo g) {
    no n;
    vertice v;

    for(n = primeiro_no(g->vertices); n; n = proximo_no(n)) {
        v = conteudo(n);
        v->visitado = 0;
    }
}

lista caminho_aumentante(grafo g) {
    no n;
    vertice v;
    lista l;

    for(n = primeiro_no(g->vertices); n; n = proximo_no(n)) {
        v = conteudo(n);
        if(!v->coberto) {
            v->visitado = 1;
            l = constroi_lista();
            if (busca_caminho(v, l, 1))
                return l;
            desvisita_vertices(g);
            destroi_lista(l, NULL);
        }
    }
    desvisita_vertices(g);
    return NULL;
    
}

void xor(lista l) {
    aresta a;
    vertice v1, v2;
    no n;

    for(n = primeiro_no(l); n; n = proximo_no(n)) {
        a = conteudo(n);
        v1 = a->vert_inicio;
        v2 = a->vert_fim;

        a->coberto = !a->coberto;
        v1->coberto = !v1->coberto;
        v2->coberto = !v2->coberto;
    }

}

void copia_vertices(grafo g, grafo e) {
    vertice v;
    no n;

    for(n = primeiro_no(g->vertices); n; n = proximo_no(n)) {
        v = conteudo(n);
        if(v->coberto)
            insere_lista(aloca_vertice(nome_vertice(v), direcionado(g)), e->vertices);
    }
}

void copia_arestas_cobertas(grafo g, grafo e) {
    vertice v, vFim, vIni;
    aresta a, novoA;
    no noV, noA;

    for(noV = primeiro_no(g->vertices); noV; noV = proximo_no(noV)) {
        v = conteudo(noV);
        for(noA = primeiro_no(v->arestas_saida); noA; noA = proximo_no(noA)) {
            a = conteudo(noA);
            if(!a->visitado && a->coberto) {
                vFim = encontra_vertice_nome(e, nome_vertice(a->vert_fim));
                vIni = encontra_vertice_nome(e, nome_vertice(a->vert_inicio));
                novoA = aloca_aresta(vIni, vFim, a->peso);
                insere_lista(novoA, vFim->arestas_entr);
                insere_lista(novoA, vIni->arestas_saida);

                //Se o grafo for direcionado, não altera o visitado, por ele só passará uma vez por cada arestas.
                a->visitado = direcionado(g) ? 0 : 1;
            }
            else
                a->visitado = 0;
        }
    }

}

grafo cria_grafo_emparelhado(grafo g) {
    grafo e = aloca_grafo(g->nome, ponderado(g), direcionado(g));
    copia_vertices(g, e);
    copia_arestas_cobertas(g, e);
    
    return e;
}

void descobre_grafo(grafo g) {
    no noV, noA;
    aresta a;
    vertice v;

    for(noV = primeiro_no(g->vertices); noV; noV = proximo_no(noV)) {
        v = conteudo(noV);
        v->coberto = 0;
        for(noA = primeiro_no(v->arestas_saida); noA; noA = proximo_no(noA)) {
            a = conteudo(noA);
            if(a->coberto) {
                a->coberto = 0;
                break;
            }
        }
    }
}

grafo emparelhamento_maximo(grafo g) {
    grafo e;
    lista l;    

    while((l = caminho_aumentante(g))) {
        xor(l);
        destroi_lista(l, NULL);
    }

    e = cria_grafo_emparelhado(g);
    //Descobre os vertices de g
    descobre_grafo(g);

    return e;
}
